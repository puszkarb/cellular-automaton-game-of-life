#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


int generujGIF(char * folder){

  char *fname = malloc(sizeof(char) * 100);
  sprintf(fname,"%s%s",folder,"/generacja1.png");

  if( access( fname, F_OK ) != -1 ){
    char * polecenie = malloc(sizeof(char) * 100);
    sprintf(polecenie,"%s%s%s","convert -delay 5 -loop 0 `ls -v $PWD/",folder,"/generacja*.png` graWZycie.gif");


    system(polecenie);
    free(polecenie);
    free(fname);
    return 0;
  }else{
        printf("Nie odnaleziono plików zawierających generacje\n" );
        free(fname);
    return 1;
  }

}
