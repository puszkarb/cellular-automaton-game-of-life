#include <stdio.h>
#include <stdlib.h>
#include <png.h>
#include <string.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include "../struktura/struktura.h"




popul_t wczytajZPNG(char *nazwa_pliku_wejsciowego, popul_t p)
{

    FILE *plik_png = fopen(nazwa_pliku_wejsciowego, "rb");
    if (plik_png == NULL) {
	printf("Błąd: [%s] nie udało się otworzyć pliku.\n",
	       nazwa_pliku_wejsciowego);

	return NULL;
    }

    char header[8];
    fread(header, 1, 8, plik_png);
    if (png_sig_cmp(header, 0, 8)) {
	printf("Błąd: [%s] nie jest rozpoznawany jako plik PNG \n",
	       nazwa_pliku_wejsciowego);

	fclose(plik_png);

	exit(1);
    }

    png_infop info_ptr;


    png_bytep *row_pointers;

    png_structp png_ptr =
	png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

    if (!png_ptr) {
	printf("Błąd: [%s] nie udało się otworzyć obrazka.\n",
	       nazwa_pliku_wejsciowego);
	fclose(plik_png);
	return NULL;
	exit(1);
    }

    png_infop end_info = png_create_info_struct(png_ptr);

    info_ptr = png_create_info_struct(png_ptr);
    if (!info_ptr) {
	printf
	    ("Błąd: [%s] nie udało się odczytać informacji o obrazku. \n",
	     nazwa_pliku_wejsciowego);
	fclose(plik_png);
	exit(1);
    }

    if (setjmp(png_jmpbuf(png_ptr))) {
	printf
	    ("Błąd: [%s] niepowodznie w trakcie ustawiania init_io. \n",
	     nazwa_pliku_wejsciowego);
	fclose(plik_png);
	exit(1);

    }

    png_init_io(png_ptr, plik_png);
    png_set_sig_bytes(png_ptr, 8);

    png_read_info(png_ptr, info_ptr);
    int szerokosc = png_get_image_width(png_ptr, info_ptr);

    int wysokosc = png_get_image_height(png_ptr, info_ptr);




    png_read_update_info(png_ptr, info_ptr);

    if (setjmp(png_jmpbuf(png_ptr))) {
	printf("Błąd: [%s] niepowodznie w trakcie read_image. \n",
	       nazwa_pliku_wejsciowego);
	fclose(plik_png);
	exit(1);
    }

    row_pointers = (png_bytep *) malloc(sizeof(png_bytep) * wysokosc);
    int i, j;
    for (i = 0; i < wysokosc; i++)
	row_pointers[i] =
	    (png_byte *) malloc(png_get_rowbytes(png_ptr, info_ptr));

    png_read_image(png_ptr, row_pointers);




    if (png_get_color_type(png_ptr, info_ptr) == PNG_COLOR_TYPE_RGB) {
	printf
	    ("Błąd: [%s] plik jest typu PNG_COLOR_TYPE_RGB, a powinien być PNG_COLOR_TYPE_RGBA - brakuje kanału \"Alpha\" \n",
	     nazwa_pliku_wejsciowego);
	png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
	for (i = 0; i < wysokosc; i++)
	    free(row_pointers[i]);
	free(row_pointers);
	fclose(plik_png);
	exit(1);
    }

    if (png_get_color_type(png_ptr, info_ptr) != PNG_COLOR_TYPE_RGBA) {
	printf
	    ("Błąd: [%s] color_type podanego pliku powinien być PNG_COLOR_TYPE_RGBA (%d) (jest: %d)\n",
	     nazwa_pliku_wejsciowego, PNG_COLOR_TYPE_RGBA,
	     png_get_color_type(png_ptr, info_ptr));

	png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
	for (i = 0; i < wysokosc; i++)
	    free(row_pointers[i]);
	free(row_pointers);
	fclose(plik_png);
	exit(1);
    }

    p = inicjalizuj(p, wysokosc, szerokosc);



    for (i = 0; i < wysokosc; i++) {
	png_byte *row = row_pointers[i];
	for (j = 0; j < szerokosc; j++) {
	    if (row[j] != 0 && row[j] != 255 && row[j] != 1) {
		printf
		    ("Błąd: [%s] - obrazek powinien być czarno biały oraz posiadać kanał alpha\n",
		     nazwa_pliku_wejsciowego);
		png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);

		for (i = 0; i < wysokosc; i++)
		    free(row_pointers[i]);
		free(row_pointers);
		sprzataj(p);
		fclose(plik_png);
		return NULL;

		exit(1);
	    }
	    p->tablica[i][j] = row[j * 4] == 0 ? 1 : 0;
	}
    }

    for (i = 0; i < wysokosc; i++)
	free(row_pointers[i]);

    free(row_pointers);


    fclose(plik_png);
    png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);




    return p;


}

popul_t wczytajZTXT(char *nazwa_pliku_wejsciowego, popul_t p)
{

    int wysokosc, szerokosc;
    FILE *plik_txt = fopen(nazwa_pliku_wejsciowego, "rb");
    if (plik_txt == NULL) {
	printf("Błąd: [%s] nie udało się otworzyć pliku.\n",
	       nazwa_pliku_wejsciowego);
	return NULL;
	exit(2);
    }
    fscanf(plik_txt, "%d %d\n", &wysokosc, &szerokosc);


    if (wysokosc <= 0 || szerokosc <= 0 || isdigit(wysokosc)
	|| isdigit(szerokosc)) {
	printf
	    ("Błąd: [%s] ; linia 1; Niepoprawny rozmiar siatki - żadna wartość nie może być równa bądź mniejsza niż 0, a także podane wartości muszą być cyframi.\n",
	     nazwa_pliku_wejsciowego);
	fclose(plik_txt);
	return NULL;
	exit(2);
    }


    p = inicjalizuj(p, wysokosc, szerokosc);


    int x;
    int y;

    while (fscanf(plik_txt, "%d %d\n", &x, &y) != EOF) {

	if (x < 0 || y < 0 || isdigit(x) || isdigit(y) || x >= szerokosc
	    || y >= wysokosc) {

	    printf
		("Błąd: [%s] podane współrzędne (%d, %d) są niepoprawne - współrzędne powinny być liczbami naturalnymi i mieszczącymi się w zakresie : x [0;%d)    y [0;%d)\n",
		 nazwa_pliku_wejsciowego, x, y, szerokosc, wysokosc);
	    fclose(plik_txt);
	    sprzataj(p);
	    return NULL;

	    exit(2);
	} else {

	    p->tablica[x][y] = 1;
	}
    }

    fclose(plik_txt);


    return p;
}


popul_t wczytajZPliku(char *nazwa_pliku_wejsciowego)
{
    if (strncmp(nazwa_pliku_wejsciowego, "", 1)) {
	char *c = strchr(nazwa_pliku_wejsciowego, '.');
	if (c == NULL) {
	    printf
		("Błąd: [%s] napotkano problem w trakcie otwierania pliku - nieprawidłowa nazwa, bądź plik nie istnieje \n",
		 nazwa_pliku_wejsciowego);
	    return NULL;
	    exit(1);
	}
	popul_t p;
	if (!strncmp(c, ".png", 4)) {
	    p = wczytajZPNG(nazwa_pliku_wejsciowego, p);
	} else if (!strncmp(c, ".txt", 4)) {
	    p = wczytajZTXT(nazwa_pliku_wejsciowego, p);
	} else {
	    printf
		("Wprowadzono nieobsługiwany format, proszę podać plik o rozszerzeniu *.png lub *.txt \n");
	    exit(1);
	}
	return p;
    } else {
	printf
	    ("Błąd: [%s] napotkano problem w trakcie otwierania pliku - nieprawidłowa nazwa, bądź plik nie istnieje \n",
	     nazwa_pliku_wejsciowego);
	exit(1);
    }
}

int zapiszDoPNG(char *folder, char *nazwa_pliku_wyjsciowego, popul_t p)
{

    png_byte color_type;
    png_byte bit_depth;
    png_bytep *row_pointers;
    bit_depth = 8;
    color_type = PNG_COLOR_TYPE_RGBA;

    char *sciezka =
	malloc(sizeof(char) *
	       (strlen(nazwa_pliku_wyjsciowego) + strlen(folder) + 5));

    sprintf(sciezka, "./%s/%s", folder, nazwa_pliku_wyjsciowego);



    char *folder_zkropka = malloc(sizeof(char) * (strlen(folder) + 3));
    sprintf(folder_zkropka, "./%s", folder);

    struct stat st = { 0 };
    if (stat(folder_zkropka, &st) == -1) {
	mkdir(folder_zkropka, 0700);
    }


    FILE *plik_png = fopen(sciezka, "wb");
    if (!plik_png) {
	printf("Błąd: [%s] nie może być otwarty do zapisu.\n",
	       nazwa_pliku_wyjsciowego);
	return 4;
    }


    png_structp png_ptr =
	png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

    if (!png_ptr) {
	printf
	    ("Błąd: operacja png_create_write_struct zakończyło się niepowodzeniem.\n");
	return 4;
    }

    png_infop info_ptr = png_create_info_struct(png_ptr);
    if (!info_ptr) {
	printf
	    ("Błąd: operacja png_create_info_struct zakończyła się niepowodzeniem\n");
	return 4;
    }

    if (setjmp(png_jmpbuf(png_ptr))) {
	printf
	    ("Błąd: init_io napotkało problemy w trakcie realizacji.\n");
	return 4;
    }

    png_init_io(png_ptr, plik_png);



    if (setjmp(png_jmpbuf(png_ptr))) {
	printf("Błąd: niepowodzenie w trakcie pisania header'a.\n");
	return 4;
    }


    png_set_IHDR(png_ptr, info_ptr, p->szerokosc, p->wysokosc,
		 bit_depth, color_type, PNG_INTERLACE_NONE,
		 PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);


    png_write_info(png_ptr, info_ptr);



    if (setjmp(png_jmpbuf(png_ptr))) {
	printf("Błąd: odczyt bitów zakończył się niepowodzeniem.\n");
	return 4;
    }


    row_pointers = (png_bytep *) calloc(p->wysokosc, sizeof(png_bytep));

    int i, j;
    for (i = 0; i < p->wysokosc; i++)
	row_pointers[i] =
	    (png_byte *) calloc(1, png_get_rowbytes(png_ptr, info_ptr));

    for (i = 0; i < p->wysokosc; i++) {
	png_byte *row = 0;
	row = row_pointers[i];
	for (j = 0; j < p->szerokosc; j++) {
	    row[j * 4] = p->tablica[i][j] == 1 ? 0 : 255;
	    row[j * 4 + 1] = p->tablica[i][j] == 1 ? 0 : 255;
	    row[j * 4 + 2] = p->tablica[i][j] == 1 ? 0 : 255;
	    row[j * 4 + 3] = p->tablica[i][j] == 1 ? 255 : 255;
	}
    }


    png_write_image(png_ptr, row_pointers);



    if (setjmp(png_jmpbuf(png_ptr))) {
	printf("Błąd: Koniec zapisu napotkał problemy.\n");
	return 4;
    }



    png_write_end(png_ptr, NULL);


    fclose(plik_png);

    for (i = 0; i < p->wysokosc; i++)
	free(row_pointers[i]);

    free(row_pointers);
    free(sciezka);
    free(folder_zkropka);

    png_destroy_write_struct(&png_ptr, &info_ptr);
    return 0;

}

int zapiszDoTXT(char *folder, char *nazwa_pliku_wyjsciowego, popul_t p)
{

    char *sciezka =
	malloc(sizeof(char) *
	       (strlen(nazwa_pliku_wyjsciowego) + strlen(folder) + 5));

    sprintf(sciezka, "./%s/%s", folder, nazwa_pliku_wyjsciowego);



    char *folder_zkropka = malloc(sizeof(char) * (strlen(folder) + 3));
    sprintf(folder_zkropka, "./%s", folder);

    struct stat st = { 0 };
    if (stat(folder_zkropka, &st) == -1) {
	mkdir(folder_zkropka, 0700);
    }
    FILE *plik_txt = fopen(sciezka, "wb");
    if (plik_txt == NULL) {
	printf
	    ("Błąd [%s] - nie udało się otworzyć pliku w trybie zapisu.\n",
	     nazwa_pliku_wyjsciowego);
	return 4;
    }
    fprintf(plik_txt, "%d %d\n", p->wysokosc, p->szerokosc);

    int i, j;
    for (i = 0; i < p->wysokosc; i++) {
	     for (j = 0; j < p->szerokosc; j++) {
	        if (p->tablica[i][j] == 1) {
		          fprintf(plik_txt, "%d %d\n", i, j);
	           }
	          }
    }
    fclose(plik_txt);
    free(sciezka);
    free(folder_zkropka);




    return 0;


}



void zapisz(char *folder, popul_t p, int numer, int czy_txt)
{


    char *nazwa_pliku_wyjsciowego = malloc(sizeof(char) * 20);

    if (czy_txt == 1) {
	sprintf(nazwa_pliku_wyjsciowego, "generacja%d.txt", numer);

    } else {
	sprintf(nazwa_pliku_wyjsciowego, "generacja%d.png", numer);

    }
    char *c = strchr(nazwa_pliku_wyjsciowego, '.');


    static int t;

    if (!strncmp(c, ".png", 4)) {

	if ((t = zapiszDoPNG(folder, nazwa_pliku_wyjsciowego, p)) != 0) {
	    printf
		("Błąd: [%s] niepowodzenie w trakcie zapisywania do obrazka.\n",
		 nazwa_pliku_wyjsciowego);

	    exit(3);
	}



    }
    if (!strncmp(c, ".txt", 4)) {

	if ((t = zapiszDoTXT(folder, nazwa_pliku_wyjsciowego, p)) != 0) {
	    printf
		("Błąd: [%s] niepowodzenie w trakcie zapisywania do pliku tekstowego.\n",
		 nazwa_pliku_wyjsciowego);

	    exit(3);
	}

    } else if (!strncmp(c, ".txt", 4) && !strncmp(c, ".png", 4)) {
	printf
	    ("Wprowadzono nieobsługiwany format, proszę podać plik o rozszerzeniu *.png lub *.txt \n");
    }
    free(nazwa_pliku_wyjsciowego);

}
