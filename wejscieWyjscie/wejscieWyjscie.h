#ifndef _WEJSCIEWYJSCIE_H
#define _WEJSCIEWYJSCIE_H

#include "../struktura/struktura.h"


popul_t wczytajZPNG (char *nazwa_pliku_wejsciowego, popul_t p);
popul_t wczytajZTXT (char *nazwa_pliku_wejsciowego, popul_t p);
popul_t wczytajZPliku (char *nazwa_pliku_wejsciowego);
int zapiszDoPNG(char * folder, char *nazwa_pliku_wyjsciowego, popul_t p);
int zapiszDoTXT(char * folder, char *nazwa_pliku_wyjsciowego, popul_t p);
void zapisz(char * folder, popul_t p , int numer, int czy_txt);

#endif
