#include <stdio.h>
#include <stdlib.h>
#include "wejscieWyjscie.h"
#include "../struktura/struktura.h"

int
sprawdz_czy_rowna (popul_t oczekiwana, popul_t podana)
{
  if (oczekiwana->wysokosc != podana->wysokosc
      || oczekiwana->szerokosc != podana->szerokosc)
    {
      return 1;
    }

  int i, j;
  for (i = 0; i < oczekiwana->wysokosc; i++)
    {
      for (j = 0; j < oczekiwana->szerokosc; j++)
	{
	  if (oczekiwana->tablica[i][j] != podana->tablica[i][j])
	    {
	      return 1;
	    }
	}
    }

  return 0;

}

void
clean (popul_t p, popul_t oczekiwana)
{
  sprzataj (p);
  sprzataj (oczekiwana);
}

int
test1 (popul_t oczekiwana, char *nazwa_pliku, popul_t podana)
{
  oczekiwana = inicjalizuj (oczekiwana, 4, 4);
  podana = wczytajZPliku (nazwa_pliku);



  //w pierwszym teście sprawdzamy poprawnosc wczytanych danych z pliku tekstowego
  /*oczekiwana->tablica wygląda w następujący sposób
     {1,0,0,0},
     {0,1,0,0},
     {0,0,1,0},
     {0,0,0,0}
   */
  int i, j;
  for (i = 0; i < 3; i++)
    {
      oczekiwana->tablica[i][i] = 1;
    }


  if (!sprawdz_czy_rowna (oczekiwana, podana))
    {
      printf ("Test 1  -------- OK\n\n");
      clean (podana, oczekiwana);
      return 0;


    }
  clean (podana, oczekiwana);

  return 1;
}

int
test2 (popul_t oczekiwana, char *nazwa_pliku, popul_t podana)
{
  oczekiwana = inicjalizuj (oczekiwana, 4, 4);
  podana = wczytajZPliku (nazwa_pliku);

  //w pierwszym teście sprawdzamy poprawnosc wczytanych danych z pliku tekstowego
  /*oczekiwana->tablica wygląda w następujący sposób
     {1,0,0,0},
     {0,1,0,0},
     {0,0,1,0},
     {0,0,0,1}
   */

  int i, j;
  for (i = 0; i < 4; i++)
    {
      oczekiwana->tablica[i][i] = 1;
    }
  if (!sprawdz_czy_rowna (oczekiwana, podana))
    {
      printf ("Test 2  -------- OK\n\n");

      clean (podana, oczekiwana);
      return 0;
    }
  clean (podana, oczekiwana);
  return 1;
}

int
test3 (popul_t podana)
{
  /* oczekujemy od programu, że wyświetli poprawny komunikat i zwróci pustą strukturę */
  podana = wczytajZPliku ("nieistniejacy_na_pewnoPLIKPNGLUtXttt.png");

  if (podana == NULL)
    {
      printf ("Test 3  -------- OK\n\n");
      return 0;
    }
  return 1;
}

int
test4 (char *nazwa_pliku, popul_t podana)
{

  //oczekujemy, że program wypisze komunikat o niepoprawnie sformatowanym pliku txt i zwróci nam pustą tablicę.
  podana = wczytajZPliku (nazwa_pliku);

  if (podana == NULL)
    {
      printf ("Test 4 -------- OK\n\n");

      return 0;
    }
  sprzataj (podana);
  return 1;
}

int
test5 (char *nazwa_pliku, popul_t podana)
{

  //oczekujemy, że program wypisze komunikat o niepoprawnie sformatowanym pliku png i zwróci nam pustą tablicę.
  podana = wczytajZPliku (nazwa_pliku);

  if (podana == NULL)
    {
      printf ("Test 5 -------- OK\n\n");

      return 0;
    }
  sprzataj (podana);
  return 1;
}

int
test6 (popul_t oczekiwana, char *nazwa_pliku, popul_t podana)
{
  //testujemy zapis tablicy do pliku txt, program nastepnie wczyta zapisany plik i porówna go z zapisywaną tablicą.

  oczekiwana = inicjalizuj (oczekiwana, 4, 4);
  int i;
  for (i = 0; i < 4; i++)
    {
      oczekiwana->tablica[i][i] = 1;
    }
  zapisz ("testowy_folder_wynikowy", oczekiwana, 0, 1);



  podana = wczytajZPliku (nazwa_pliku);

  if (podana != NULL && !sprawdz_czy_rowna (oczekiwana, podana))
    {
      printf ("Test 6  -------- OK\n\n");

      clean (podana, oczekiwana);
      return 0;
    }

  if (podana != NULL)
    {
      clean (podana, oczekiwana);
      return 1;
    }
  sprzataj (oczekiwana);
  return 1;
}

int
test7 (popul_t oczekiwana, char *nazwa_pliku, popul_t podana)
{
  //testujemy zapis tablicy do pliku png, program nastepnie wczyta zapisany plik i porówna go z zapisywaną tablicą.

  oczekiwana = inicjalizuj (oczekiwana, 4, 4);
  int i, j;
  for (i = 0; i < 4; i++)
    {
      oczekiwana->tablica[i][i] = 1;
    }
  zapisz ("testowy_folder_wynikowy", oczekiwana, 0, 0);




  podana = wczytajZPliku (nazwa_pliku);

  if (podana != NULL && !sprawdz_czy_rowna (oczekiwana, podana))
    {
      printf ("Test 7  -------- OK\n\n");

      clean (podana, oczekiwana);
      return 0;
    }

  if (podana != NULL)
    {
      clean (podana, oczekiwana);
      return 1;
    }
  sprzataj (oczekiwana);
  return 1;
}






int
main (int argc, char **argv)
{


  popul_t oczekiwana = NULL;
  popul_t p = NULL;



  if (test1 (oczekiwana, "test/testowy.txt", p))
    {
      printf ("Test 1 -------- NIEPOWODZENIE\n\n");
    }
  if (test2 (oczekiwana, "test/test.png", p))
    {
      printf ("Test 2 -------- NIEPOWODZENIE\n\n");
    }
  if (test3 (p))
    {
      printf ("Test 3 -------- NIEPOWODZENIE\n\n");
    }
  if (test4 ("test/test_wrong.txt", p))
    {
      printf ("Test 4 -------- NIEPOWODZENIE\n\n");
    }
  if (test5 ("test/test_wrong.png", p))
    {
      printf ("Test 5 -------- NIEPOWODZENIE\n\n");
    }
  if (test6 (oczekiwana, "testowy_folder_wynikowy/generacja0.txt", p))
    {
      printf ("Test 6 -------- NIEPOWODZENIE\n\n");
    }
  if (test7 (oczekiwana, "testowy_folder_wynikowy/generacja0.png", p))
    {
      printf ("Test 7 -------- NIEPOWODZENIE\n\n");
    }


  return 0;
}
