#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "logikaGry/logikaGry.h"
#include "wejscieWyjscie/wejscieWyjscie.h"
#include "generatrGIF/generatorGIF.h"
#include "struktura/struktura.h"
#include "obslugaGry/obsluga.h"

int
main (int argc, char **argv)
{
  if (argc == 1)
    drukujInstrukcje ();

  argWej_t argumentyWejsciowe = inicjujDomyslnymi (argumentyWejsciowe);;
  argumentyWejsciowe = sprawdzArgumenty (argumentyWejsciowe, argc, argv);

  if (argumentyWejsciowe == NULL)
    {
      czyscArgumenty (argumentyWejsciowe);
      return -1;
    }

  popul_t populacja = wczytajZPliku (argumentyWejsciowe->nazwaPlikuWej);
  if (populacja == NULL)
    {
      czyscArgumenty (argumentyWejsciowe);
      return -2;
    }

  if (sprawdzCzyFolderWyjIstnieje (argumentyWejsciowe->folderWyjsciowy) == 1)
    czyscFolderWyjsciowy (argumentyWejsciowe->folderWyjsciowy);

  startGry (populacja, argumentyWejsciowe);
  generujGIF (argumentyWejsciowe->folderWyjsciowy);
  jesliTrzebaZostawTylkoOstatniaGeneracje (argumentyWejsciowe);

  sprzataj (populacja);
  czyscArgumenty (argumentyWejsciowe);

  return 0;
}
