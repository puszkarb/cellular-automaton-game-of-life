grawzycie: obsluga logika wejsciewyjscie generatr struktura graWZycie.o
	gcc -ggdb -o graWZycie graWZycie.o logikaGry/logikaGry.o wejscieWyjscie/wejscieWyjscie.o generatrGIF/generatorGIF.o struktura/struktura.o obslugaGry/obsluga.o -lpng

obsluga:
	+$(MAKE) -C obslugaGry/

wejsciewyjscie:
	+$(MAKE) -C wejscieWyjscie/

logika:
	+$(MAKE) -C logikaGry/

generatr:
	+$(MAKE) -C generatrGIF/

struktura:
	+$(MAKE) -C struktura/

graWZycie.o: graWZycie.c

testValgrind: graWZycie
	valgrind ./graWZycie wejscieWyjscie/test/pociag.png --g 200 --wb 2
testBezArg: graWZycie
	./graWZycie
testInnaNazwaFolderu: graWZycie
	./graWZycie wejscieWyjscie/test/pociag.png --s nowaNazwa
testZapisWszystkich: graWZycie
	./graWZycie wejscieWyjscie/test/pociag.png --s testZapisuAll --all
testTxt: graWZycie
	./graWZycie wejscieWyjscie/test/pociag.png --s testTxt --txt
testBlednaNazwaPliku: graWZycie
	./graWZycie plikKtoryNieIstnieje
testPlikWTxt: graWZycie
	./graWZycie wejscieWyjscie/test/testowy.txt
testTxtWrong: graWZycie
	./graWZycie wejscieWyjscie/test/test_wrong.txt
testPngWrong: graWZycie
	./graWZycie wejscieWyjscie/test/test_wrong.png
clean:
	rm graWZycie
	rm graWZycie.gif
	rm generacje/*
	rmdir generacje
