#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "logikaGry.h"
#include "../struktura/struktura.h"
#include "../wejscieWyjscie/wejscieWyjscie.h"

int
sprawdz (popul_t poprawna, popul_t podana)
{
  int i, j;
  for (i = 0; i < poprawna->wysokosc; i++)
    for (j = 0; j < poprawna->szerokosc; j++)
      {
	if (poprawna->tablica[i][j] != podana->tablica[i][j])
	  return 1;
      }
  return 0;
}


void
testuj (int iloscDoSpr, int warBrzeg, char *folderZPoprawnymi)
{
  int i;
  char *nazwaGeneracjiDoPorownania =
    malloc (sizeof (char) * (strlen (folderZPoprawnymi) + 20));
  popul_t poprawna;
  popul_t podana;
  sprintf (nazwaGeneracjiDoPorownania, "%s/generacja1.png",
	   folderZPoprawnymi);
  poprawna = wczytajZPliku (nazwaGeneracjiDoPorownania);
  podana = inicjalizuj (podana, poprawna->wysokosc, poprawna->szerokosc);
  //tablica stanow dla ktorej mamy poprawne obrazy png dla kazdego wb
  int j;
  for (i = 0; i < poprawna->wysokosc; i++)
    for (j = 0; j < poprawna->szerokosc; j++)
      {
	if (i == j)
	  podana->tablica[i][j] = 1;
	else
	  podana->tablica[i][j] = 0;
      }
  // podana -> tablica = poczatkowyStan;  
  for (i = 1; i <= iloscDoSpr; i++)
    {
      if (i > 1)
	{
	  sprintf (nazwaGeneracjiDoPorownania, "%s/generacja%d.png",
		   folderZPoprawnymi, i);
	  sprzataj (poprawna);
	  poprawna = wczytajZPliku (nazwaGeneracjiDoPorownania);

	}
      podana = sprawdzStanPopulacji (podana, warBrzeg);

      if (sprawdz (poprawna, podana) == 0)
	printf ("Test dla war brzeg: %d generacja %d --------OK\n\n",
		warBrzeg, i);
      else
	printf ("Test dla war brzeg: %d generacja %d --------BLAD\n\n",
		warBrzeg, i);

    }
  free (nazwaGeneracjiDoPorownania);
  sprzataj (podana);
  sprzataj (poprawna);
}

int
main (int argc, char **argv)
{
  testuj (5, 1, "poprawneGeneracje");
  printf("\n");
  testuj (5, 2, "poprawneGeneracje2");
  printf("\n");
  testuj (5, 3, "poprawneGeneracje3");
  return 0;
}
