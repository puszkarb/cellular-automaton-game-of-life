#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../struktura/struktura.h"

int
zmienStanKomorki (int ileSasiadow, int obecnyStan)
{
  if (obecnyStan == 0 && ileSasiadow == 3)
    return 1;
  if (obecnyStan == 1 && (ileSasiadow == 2 || ileSasiadow == 3))
    return 1;
  return 0;
}

int **
krawedzie (popul_t popul, int **nowyStan, int sposob)
{
  //lewa i prawa
  int i, j, wys, szer, ile = 0;
  wys = popul->wysokosc;
  szer = popul->szerokosc;
  int **populacja = popul->tablica;
  for (j = 0; j < szer; j += (szer - 1))
    {
      for (i = 1; i < wys - 1; i++)
	{
	  ile = 0;
	  if (j == 0)
	    {
	      if (populacja[i][j + 1] == 1)
		ile++;
	      if (populacja[i - 1][j + 1] == 1)
		ile++;
	      if (populacja[i + 1][j + 1] == 1)
		ile++;
	      if (sposob == 1)
		{
		  if (populacja[i][szer - 1] == 1)
		    ile++;
		  if (populacja[i - 1][szer - 1] == 1)
		    ile++;
		  if (populacja[i + 1][szer - 1] == 1)
		    ile++;
		}
	      if (sposob == 3)
		ile = ile * 2;
	    }
	  if (j == (szer - 1))
	    {
	      if (populacja[i][j - 1] == 1)
		ile++;
	      if (populacja[i - 1][j - 1] == 1)
		ile++;
	      if (populacja[i + 1][j - 1] == 1)
		ile++;
	      if (sposob == 1)
		{
		  if (populacja[i][0] == 1)
		    ile++;
		  if (populacja[i - 1][0] == 1)
		    ile++;
		  if (populacja[i + 1][0] == 1)
		    ile++;
		}
	      if (sposob = 3)
		ile = ile * 2;
	    }
	  if (populacja[i - 1][j] == 1)
	    ile++;
	  if (populacja[i + 1][j] == 1)
	    ile++;
	  nowyStan[i][j] == zmienStanKomorki (ile, populacja[i][j]);
	}
    }

  //gora i dol
  for (j = 0; j < wys; j += (wys - 1))
    {
      for (i = 1; i < szer - 1; i++)
	{
	  ile = 0;
	  if (j == 0)
	    {
	      if (populacja[j + 1][i - 1] == 1)
		ile++;
	      if (populacja[j + 1][i] == 1)
		ile++;
	      if (populacja[j + 1][i + 1] == 1)
		ile++;
	      if (sposob == 1)
		{
		  if (populacja[wys - 1][i - 1] == 1)
		    ile++;
		  if (populacja[wys - 1][i] == 1)
		    ile++;
		  if (populacja[wys - 1][i + 1] == 1)
		    ile++;
		}
	      if (sposob == 3)
		ile = ile * 2;
	    }
	  if (j == (wys - 1))
	    {
	      if (populacja[j - 1][i - 1] == 1)
		ile++;
	      if (populacja[j - 1][i] == 1)
		ile++;
	      if (populacja[j - 1][i + 1] == 1)
		ile++;
	      if (sposob == 1)
		{
		  if (populacja[0][i - 1] == 1)
		    ile++;
		  if (populacja[0][i] == 1)
		    ile++;
		  if (populacja[0][i + 1] == 1)
		    ile++;
		}
	      if (sposob == 3)
		ile = ile * 2;
	    }
	  if (populacja[j][i - 1] == 1)	//gora lub dol sasiad  z lewej
	    ile++;
	  if (populacja[j][i + 1] == 1)	//gora lob dol sasiad z prawej
	    ile++;
	  nowyStan[i][j] == zmienStanKomorki (ile, populacja[j][i]);
	}
    }
  return nowyStan;
}

int
lewyGorny (popul_t popul, int sposob)
{
  int wys, szer, ile = 0;
  wys = popul->wysokosc;
  szer = popul->szerokosc;
  int **populacja = popul->tablica;

  if (populacja[0][1] == 1)
    ile++;
  if (populacja[1][0] == 1)
    ile++;
  if (populacja[1][1] == 1)
    ile++;

  if (sposob == 1)
    {
      if (populacja[wys - 1][szer - 2] == 1)
	ile++;
      if (populacja[wys - 2][szer - 1] == 1)
	ile++;
      if (populacja[wys - 2][szer - 2] == 1)
	ile++;
    }
  if (sposob == 3)
    {
      ile = ile * 2;
    }
  return ile;
}

int
prawyGorny (popul_t popul, int sposob)
{
  int wys, szer, ile = 0;
  wys = popul->wysokosc;
  szer = popul->szerokosc;
  int **populacja = popul->tablica;

  if (populacja[0][szer - 2] == 1)
    ile++;
  if (populacja[1][szer - 1] == 1)
    ile++;
  if (populacja[1][szer - 2] == 1)
    ile++;

  if (sposob == 1)
    {
      if (populacja[wys - 1][2] == 1)
	ile++;
      if (populacja[wys - 2][1] == 1)
	ile++;
      if (populacja[wys - 2][2] == 1)
	ile++;
    }
  if (sposob == 3)
    {
      ile = ile * 2;
    }
  return ile;
}


int
lewyDolny (popul_t popul, int sposob)
{
  int wys, szer, ile = 0;
  wys = popul->wysokosc;
  szer = popul->szerokosc;
  int **populacja = popul->tablica;

  if (populacja[wys - 2][0] == 1)
    ile++;
  if (populacja[wys - 2][1] == 1)
    ile++;
  if (populacja[wys - 1][1] == 1)
    ile++;

  if (sposob == 1)
    {
      if (populacja[0][szer - 2] == 1)
	ile++;
      if (populacja[1][szer - 1] == 1)
	ile++;
      if (populacja[1][szer - 2] == 1)
	ile++;
    }
  if (sposob == 3)
    {
      ile = ile * 2;
    }
  return ile;
}

int
prawyDolny (popul_t popul, int sposob)
{
  int wys, szer, ile = 0;
  wys = popul->wysokosc;
  szer = popul->szerokosc;
  int **populacja = popul->tablica;

  if (populacja[wys - 1][szer - 2] == 1)
    ile++;
  if (populacja[wys - 2][szer - 1] == 1)
    ile++;
  if (populacja[wys - 2][szer - 2] == 1)
    ile++;
  if (sposob == 1)
    {
      if (populacja[0][1] == 1)
	ile++;
      if (populacja[1][0] == 1)
	ile++;
      if (populacja[1][1] == 1)
	ile++;
    }
  if (sposob == 3)
    {
      ile = ile * 2;
    }
  return ile;
}

popul_t
sprawdzStanPopulacji (popul_t popul, int sposob)
{
  int i, j, wys, szer;
  wys = popul->wysokosc;
  szer = popul->szerokosc;
  int **populacja = popul->tablica;

  popul_t tymczasowaPopul = inicjalizuj (tymczasowaPopul, wys, szer);
  int **nowyStan = tymczasowaPopul->tablica;
  nowyStan = krawedzie (popul, nowyStan, sposob);

  nowyStan[0][0] =
    zmienStanKomorki (lewyGorny (popul, sposob), populacja[0][0]);

  nowyStan[0][szer - 1] =
    zmienStanKomorki (prawyGorny (popul, sposob), populacja[0][szer - 1]);

  nowyStan[wys - 1][szer - 1] =
    zmienStanKomorki (prawyDolny (popul, sposob),
		      populacja[wys - 1][szer - 1]);

  nowyStan[wys - 1][0] =
    zmienStanKomorki (lewyDolny (popul, sposob), populacja[wys - 1][0]);

  for (i = 1; i < wys - 1; i++)
    {
      for (j = 1; j < szer - 1; j++)
	{
	  int sasiedzi = 0;
	  if (populacja[i - 1][j - 1] == 1)
	    sasiedzi++;
	  if (populacja[i][j - 1] == 1)
	    sasiedzi++;
	  if (populacja[i + 1][j - 1] == 1)
	    sasiedzi++;
	  if (populacja[i - 1][j] == 1)
	    sasiedzi++;
	  if (populacja[i + 1][j] == 1)
	    sasiedzi++;
	  if (populacja[i - 1][j + 1] == 1)
	    sasiedzi++;
	  if (populacja[i][j + 1] == 1)
	    sasiedzi++;
	  if (populacja[i + 1][j + 1] == 1)
	    sasiedzi++;
	  nowyStan[i][j] = zmienStanKomorki (sasiedzi, populacja[i][j]);
	}
    }

  for (i = 0; i < wys; i++)
    memcpy (populacja[i], nowyStan[i], szer * sizeof (int));

  sprzataj (tymczasowaPopul);
  return popul;
}
