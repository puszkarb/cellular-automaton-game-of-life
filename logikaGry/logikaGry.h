#ifndef _LOGIKAGRY_H_IS_INCLUDED_ 
#define _LOGIKAGRY_H_IS_INCLUDED_
#include "../struktura/struktura.h"

int zmienStanKomorki ( int ileSasiadow, int obecnyStan );

int** krawedzie( popul_t popul, int **nowyStan, int sposob);

int lewyGorny( popul_t popul, int sposob );

int prawyGorny( popul_t popul, int sposob );

int lewyDolny( popul_t popul, int sposob );

int prawyDolny( popul_t popul, int sposob );

popul_t sprawdzStanPopulacji ( popul_t popul, int sposob );

#endif
