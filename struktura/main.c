#include <stdio.h>
#include <stdlib.h>

#include "struktura.h"

int main(int argc, char ** argv){

  popul_t pop;

  pop = inicjalizuj(pop, 10 , 10);
  if (pop == NULL) {
    printf("NIEPOWODZENIE TESTU\n");
    return 1;
  }
  sprzataj(pop);
  if (pop != NULL) {
    printf("TEST OK\n" );
  }

  return 0;
}
