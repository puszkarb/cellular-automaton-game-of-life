#ifndef _STRUKTURA_H
#define _STRUKTURA_H


typedef struct populacja_struktura{
  int **tablica;
  int wysokosc;
  int szerokosc;
}*popul_t;

popul_t inicjalizuj(popul_t p, int wysokosc, int szerokosc);
void sprzataj(popul_t p);

#endif
