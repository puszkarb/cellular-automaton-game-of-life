#include <stdio.h>
#include <stdlib.h>
#include "struktura.h"


popul_t inicjalizuj(popul_t p, int wysokosc, int szerokosc){
  p = malloc(sizeof(*p));
  p->wysokosc = wysokosc;
  p->szerokosc = szerokosc;
  p->tablica = calloc(p->wysokosc , sizeof(int *));
  int i;
  for ( i = 0; i < p->wysokosc; i++) {
    p->tablica[i] = calloc(p->szerokosc, sizeof(int));
  }
  return p;
}


void sprzataj(popul_t p){
  int i;
  for(i = 0; i < p->wysokosc; i++){
    free(p->tablica[i]);
  }
  free(p->tablica);
  free(p);


}
