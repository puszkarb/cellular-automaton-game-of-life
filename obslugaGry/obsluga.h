#ifndef _OBSLUGAGRY_H
#define _OBSLUGAGRY_H
#include "../struktura/struktura.h"

typedef struct arg_wejsciowe{
	char *nazwaPlikuWej;
  	int liczbaGen;
  	int warBrzeg;
	int wyj;
	int all;
	int czyPodanoFolderWyj;
	char *folderWyjsciowy;
	
}*argWej_t;

void drukujInstrukcje();

argWej_t inicjujDomyslnymi ( argWej_t ar );

char *tworzNazwe( char *nazwa ); 

argWej_t sprawdzArgumenty( argWej_t argumenty, int argc, char **argv);

int sprawdzCzyFolderWyjIstnieje(char *nazwa);

void czyscFolderWyjsciowy( char *folderWyjsciowy );

void startGry(popul_t populacja, argWej_t argumenty);

void jesliTrzebaZostawTylkoOstatniaGeneracje( argWej_t argumenty );

argWej_t czyscArgumenty( argWej_t argumenty );

#endif
