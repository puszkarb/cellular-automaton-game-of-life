#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <dirent.h>

#include "../logikaGry/logikaGry.h"
#include "../wejscieWyjscie/wejscieWyjscie.h"
#include "../struktura/struktura.h"
#include "obsluga.h"

void
drukujInstrukcje ()
{
  printf ("Witaj w symulatorze gry w zycie, oto krotki opis "
	  "dzialania programu oraz instrukcja obslugi:\n\n"
	  "Program ten ma za zadanie przeprowadzenie symulacji gry w zycie dla zadanego przez uzytkownika stanu poczatkowego.\n"
	  "Poczatkowy stan gry (Planszy) przekazywany jest do programu w postaci pliku w formacie *.png lub *.txt\n"
	  "Plik w formacie *.txt musi zawierac w pierwszej lini wysokosc oraz szerokosc planszy oddzielone spacja\n"
	  "w kolejnych liniach odpowiednio wspolrzedne x oraz y " "zywych"
	  " komorek. Plik w formacie *.png musi zawierac\n"
	  "biale i czarne pixele odpowiadajace stanom komorek - bialy:martwa, czarny:zywa\n\n"
	  "Instrukcja wywolania programu\n \n"
	  "\t\tgraWZycie [WEJ][ -–g (GEN?)][ -–wb (WAR BRZEG?)][–- txt?][-–all (WSZYSTKIE?)] [-–s (KATALOG?)]\n\n"
	  "gdzie:\n\tWEJ - nazwa pliku z poczatkowym rozkładem komórek w formacie"
	  " *.txt lub *.png, jest to KONIECZNY argument wywołania\n\t"
	  "--g GEN - liczba generacji\n\t--wb WAR BRZEG warunek brzegowy, przyjmuje 1,2 lub 3 gdzie\n\t\t"
	  "1-periodyczny(domyslnie), \n\t\t2-pochlaniajacy, \n\t\t3-odbijajacy\n\t"
	  "--txt - rozszarzenie pliku wyjsciowego z generacja, domyślnie *.png, aby zmienić na *.txt nalezy wowolac program z flaga --txt\n\t"
	  "--all/--a po dodaniu tej flagi zostaja zapisane wszystkie generacje, bez niej zostaje zapisana tylko ostatnia oraz plik gif\n\t"
	  "--s nazwa folderu z generacja\n\nPrzykladowe wywolanie:\n\n\t"
	  "./graWZycie wejscieWyjscie/test/pociag.png --g 50 --wb 1\n");
  exit (-1);
}

argWej_t
inicjujDomyslnymi (argWej_t ar)
{
  ar = malloc (sizeof (*ar));
  if (ar == NULL)
    {
      fprintf (stderr,
	       "Blad alokacji pamieci podczas inicjacji struktury argumentow\n");
      free (ar);
      exit (-1);
    }
  ar->nazwaPlikuWej = NULL;
  ar->liczbaGen = 10;
  ar->warBrzeg = 1;
  ar->wyj = 0;			//0 czyli png
  ar->all = 0;			//nie
  ar->czyPodanoFolderWyj = 0;	//nie
  ar->folderWyjsciowy = NULL;
  return ar;
}

char *
tworzNazwe (char *nazwa)
{
  char *zwracanaNazwa = malloc (sizeof (char) * strlen (nazwa) + 1);
  if (zwracanaNazwa == NULL)
    {
      fprintf (stderr, "Blad alokacji pamieci podczas tworzenia nazwy %s\n",
	       nazwa);
      exit (-1);
    }
  zwracanaNazwa = strcpy (zwracanaNazwa, nazwa);
  return zwracanaNazwa;
}


argWej_t
sprawdzArgumenty (argWej_t argumenty, int argc, char **argv)
{
  argumenty->nazwaPlikuWej = tworzNazwe (argv[1]);
  int i;
  for (i = 2; i < argc; i++)
    {
      if (strcmp (argv[i], "--g") == 0)
	{
	  if (atoi (argv[++i]) > 0)
	    argumenty->liczbaGen = atoi (argv[i]);
	  else
	    {
	      printf
		("Za flaga --g podales nieprawidlowy argument, aby poprawnie wybrac liczbe generacji uzyj flagi w nastepujacy sposob"
		 "\n--g <LiczbaCalkowita>\\np --g 5");
	      czyscArgumenty (argumenty);
	      return NULL;
	    }
	}
      else if (strcmp (argv[i], "--wb") == 0)
	{
	  if (atoi (argv[i + 1]) == 1 || atoi (argv[i + 1]) == 2
	      || atoi (argv[i + 1]) == 3)
	    argumenty->warBrzeg = atoi (argv[++i]);
	  else
	    {
	      printf
		("Za flaga --wb podales nieprawidlowy argument, aby poprawnie wybrac warunek brzegowy uzyj flagi w nastepujacy sposob"
		 "\n--g <1-war przenikajacy lub 2-war wygaszajacy lub 3-war odbijajacy>\\np --wb 3");
	      czyscArgumenty (argumenty);
	      return NULL;
	    }
	}
      else if (strcmp (argv[i], "--txt") == 0)
	argumenty->wyj = 1;
      else if (strcmp (argv[i], "--all") == 0 || strcmp (argv[i], "--a") == 0)
	argumenty->all = 1;
      else if (strcmp (argv[i], "--s") == 0)
	{
	  argumenty->folderWyjsciowy = tworzNazwe (argv[++i]);
	  argumenty->czyPodanoFolderWyj = 1;
	}

    }
  if (argumenty->czyPodanoFolderWyj == 0)
    argumenty->folderWyjsciowy = tworzNazwe ("generacje");
  return argumenty;

}

int
sprawdzCzyFolderWyjIstnieje (char *nazwa)
{
  DIR *dir;
  if (access (nazwa, F_OK) != -1)
    if ((dir = opendir (nazwa)) != NULL)
      {
	closedir (dir);
	return 1;
      }
  return 0;
}

void
czyscFolderWyjsciowy (char *folderWyjsciowy)
{

  char *czyscKatalog =
    malloc (sizeof (char) * (strlen (folderWyjsciowy) + 7));
  if (czyscKatalog == NULL)
    {
      fprintf (stderr,
	       "Blad alokacji pamieci podczas tworzenia komendy do czyszczenia katologu\n");
      free (czyscKatalog);
      exit (-1);
    }
  sprintf (czyscKatalog, "rm %s/*", folderWyjsciowy);
  system (czyscKatalog);
  free (czyscKatalog);
}

void
startGry (popul_t populacja, argWej_t argumenty)
{
  int i;
  for (i = 0; i < argumenty->liczbaGen; i++)
    {
      populacja = sprawdzStanPopulacji (populacja, argumenty->warBrzeg);
      zapisz (argumenty->folderWyjsciowy, populacja, i + 1, argumenty->wyj);
    }
}

void
jesliTrzebaZostawTylkoOstatniaGeneracje (argWej_t argumenty)
{
  if (argumenty->all == 0)
    {
      int k;
      char *usunTaGen =
	malloc (sizeof (char) * (strlen (argumenty->folderWyjsciowy) + 22));
      if (usunTaGen == NULL)
	{
	  fprintf (stderr,
		   "Blad alokacji pamieci podczas tworzenia komendy do czyszczenia generacji\n");
	  free (usunTaGen);
	  exit (-3);
	}
      for (k = 1; k < argumenty->liczbaGen; k++)
	{
	  if (argumenty->wyj == 1)
	    {
	      sprintf (usunTaGen, "rm %s/generacja%d.txt",
		       argumenty->folderWyjsciowy, k);
	    }
	  else
	    {
	      sprintf (usunTaGen, "rm %s/generacja%d.png",
		       argumenty->folderWyjsciowy, k);
	    }
	  system (usunTaGen);
	}
      free (usunTaGen);
    }
}


argWej_t
czyscArgumenty (argWej_t argumenty)
{
  free (argumenty->folderWyjsciowy);
  free (argumenty->nazwaPlikuWej);
  free (argumenty);
  return NULL;
}
