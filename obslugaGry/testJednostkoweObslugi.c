#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../logikaGry/logikaGry.h"
#include "../wejscieWyjscie/wejscieWyjscie.h"
#include "../struktura/struktura.h"
#include "obsluga.h"

int
testCzyscArgumenty ()
{
  argWej_t ar;
  ar = malloc (sizeof (*ar));
  ar->nazwaPlikuWej = malloc (sizeof (char) * 20);

  ar->liczbaGen = 10;
  ar->warBrzeg = 1;
  ar->wyj = 0;			//0 czyli png
  ar->all = 0;			//nie
  ar->czyPodanoFolderWyj = 0;	//nie
  ar->folderWyjsciowy = malloc (sizeof (char) * 20);
  ar = czyscArgumenty (ar);
  if (ar == NULL)
    {
      printf ("\nTest " "czyscArgumenty" " -------- OK\n\n");
      return 0;
    }
  free (ar->nazwaPlikuWej);
  free (ar->folderWyjsciowy);
  return 1;

}

int
testInicjujDomyslnymi ()
{
  argWej_t arTest;
  arTest = inicjujDomyslnymi (arTest);
  if (arTest != NULL)
    {
      printf ("\nTest inicjujDomyslnymi -------- OK\n\n");
      czyscArgumenty (arTest);
      return 0;
    }
  return 1;

}


int
testTworzNazwe (char *nazwa)
{
  char *stworzonaNazwa = tworzNazwe (nazwa);
  if (stworzonaNazwa != NULL)
    printf ("\nTest tworzNazwe -------- OK\n\n");
  free (stworzonaNazwa);
  return 0;
  free (stworzonaNazwa);
  return 1;

}

int
testSprawdzArgWejPoprawny (int argc, char **argv)
{
  argWej_t arTest;
  arTest = inicjujDomyslnymi (arTest);
  arTest = sprawdzArgumenty (arTest, argc, argv);
  if (arTest != NULL)
    {
      printf ("\nTest SprawdzArgWej POPRAWNE DANE -------- OK\n\n");
      czyscArgumenty (arTest);
      return 0;
    }
  return 1;
}


int
testSprawdzArgWejNiePoprawny (int argc, char **argv)
{
  argWej_t arTest;
  arTest = inicjujDomyslnymi (arTest);
  arTest = sprawdzArgumenty (arTest, argc, argv);
  if (arTest == NULL)
    {
      printf ("\n\nTest SprawdzArgWej ZLE DANE -------- OK\n\n");
      return 0;
    }
  czyscArgumenty (arTest);
  return 1;
}

int
main (int argc, char **argv)
{
  char *poprawneDane[] = { "plikWej.txt", "--all", "--wb", "1" };
  char *zleDane[] = { "plikWej.txt", "--all", "--wb", "10" };

  if (testCzyscArgumenty ())
    printf ("\nTest czyscArgumenty -------- NIEPOWODZENIE\n\n");
  if (testInicjujDomyslnymi ())
    printf ("\nTest inicjujDomyslnymi -------- NIEPOWODZENIE\n\n");
  if (testTworzNazwe ("nazwaTestowa"))
    printf ("\nTest tworzNazwe -------- NIEPOWODZENIE\n\n");
  if (testSprawdzArgWejPoprawny (5, poprawneDane))
    printf ("\nTest SprawdzArgWej POPRAWNE DANE -------- NIEPOWODZENIE\n\n");
  if (testSprawdzArgWejNiePoprawny (5, zleDane))
    printf ("\nTest SprawdzArgWej ZLE DANE -------- NIEPOWODZENIE\n\n");


  return 0;
}
