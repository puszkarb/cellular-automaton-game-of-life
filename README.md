# Cellular Automaton - Game of Life

## Description
Cellular Automaton is a project written in C with additional use of Linux's terminal commands to generate GIF. User can pass data to our program both in TXT and PNG formats,           
however if second is chosen it has to contain the Alpha channel. There is almost no limitation in size of given image, program will operate on any income provided, nonetheless amount of time needed will increase along with size.
There are 3 different behaviour's patterns which control evolution flow near edges. We will use glider as an example:   
- edge is killing coliding glider,            
- glider is bouncing off the edge,                
- glider is passing through the ednge and appear on the other side as if it was looped.                

Whole project was created for educational purposes, and what goes with that is variables names and instructions are available only in Polish.           

## First use / manual

1) Open the terminal and go to the project directory            
2) /make              
3) Run program without any arguments by simply typing ./graWZycie - instruction manual will be displayed. (As it was mentioned above, due to the fact that project was never thought to be publicated, instruction is available only in Polish language)                
4) Enjoy! Valgrind tests are also included in the makefile, to proceed them type /make testValgrind. However, other tests are also included - they should cover vast majority of predictable issues so feel free to play with them.     

 